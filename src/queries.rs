use std::collections::HashMap;
use actix_web::{HttpRequest, HttpResponse, web};
use crate::utils::db::{Pool, PoolConn};

lazy_static! {
    static ref QUERIES: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        m.insert("get_accounts", "accounts");
        m.insert("get_cards", "cards");
        m
    };
}

pub async fn query_handler(
    req: HttpRequest,
    path: web::Path<String>
) -> HttpResponse {
    let db_pool = req.app_data::<Pool>().unwrap().get().unwrap();
    query_handler_internal(db_pool, path.into_inner().as_str())
}

fn query_handler_internal(db_pool: PoolConn, query: &str) -> HttpResponse {
    let mut client = db_pool;
    match QUERIES.get(query) {
        None => HttpResponse::NotFound().body("No such query"),
        Some(q_str) => {
            let query_str = format!("select COALESCE(json_agg({}), '[]'::json)::text as json_row from {}", q_str, q_str);
            let result = client
                .query(query_str.as_str(), &[])
                .expect("db error");
            let json_rows: String = result.get(0).unwrap().get(0);
            HttpResponse::Ok().body(json_rows)
        }
    }
}
