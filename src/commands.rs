use actix_web::{HttpRequest, HttpResponse, web};
use actix_web::error::{Error, ErrorNotFound};
use crate::utils::db::{Pool, PoolConn};
use crate::users::aggregate::{CreateAccountAggregate, CreateCardAggregate};

pub async fn command_handler(
    req: HttpRequest,
    path: web::Path<String>,
    body: web::Bytes
) -> HttpResponse {
    let command = path.into_inner();
    let body = String::from_utf8(body.to_vec()).unwrap();
    let db_pool = req.app_data::<Pool>().unwrap().get().unwrap();
    match command_handler_internal(db_pool, command.as_str(), body.as_str()) {
        Ok(_) => HttpResponse::Ok().body(""),
        Err(e) => HttpResponse::InternalServerError().body(e.to_string()),
    }
}

fn command_handler_internal(db_pool: PoolConn, command: &str, body: &str) -> Result<(), Error> {
    match command {
        "insert_account" => CreateAccountAggregate::execute(db_pool, body),
        "insert_card" => CreateCardAggregate::execute(db_pool, body),
        _ => return Result::Err(ErrorNotFound("command not found")),
    };
    Ok(())
}
