extern crate env_logger;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate lazy_static;

mod commands;
mod queries;
mod users;
mod utils;

use std::io::Result;
use actix_web::{App, HttpServer, web};

#[actix_rt::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    HttpServer::new(|| App::new()
        .app_data(utils::db::initialize_connection_pool())
        .service(web::scope("/api")
            .route("/{query}", web::get().to(queries::query_handler))
            .route("/command/{command}", web::post().to(commands::command_handler))
        )
    ).bind("0.0.0.0:5555")?.run().await
}
