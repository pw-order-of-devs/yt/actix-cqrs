use crate::users::domain::{Account, Card};
use crate::users::repository as users_repo;
// use logging_repo;
use crate::utils::db::PoolConn;

pub struct CreateAccountAggregate {}

impl CreateAccountAggregate {
    pub fn execute(db_pool: PoolConn, body: &str) {
        let item: Account = serde_json::from_str(body).unwrap();
        users_repo::create_account(db_pool, item);
        // logging_repo::log_activity(...)
        // insert/update additional data
    }
}

pub struct CreateCardAggregate {}

impl CreateCardAggregate {
    pub fn execute(db_pool: PoolConn, body: &str) {
        let item: Card = serde_json::from_str(body).unwrap();
        users_repo::create_card(db_pool, item);
        // logging_repo::log_activity(...)
    }
}
