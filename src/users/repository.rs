use crate::users::domain::{Account, Card};
use crate::utils::db::PoolConn;

pub fn create_account(mut db_pool: PoolConn, item: Account) {
    db_pool
        .execute("insert into accounts values ($1)", &[&item.username])
        .expect("db error");
}

pub fn create_card(mut db_pool: PoolConn, item: Card) {
    db_pool
        .execute("insert into cards values ($1)", &[&item.name])
        .expect("db error");
}