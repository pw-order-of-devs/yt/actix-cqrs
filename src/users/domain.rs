#[derive(Serialize, Deserialize)]
pub struct Account { pub username: String }

#[derive(Serialize, Deserialize)]
pub struct Card { pub name: String }