pub type ConnectionManager = r2d2_postgres::PostgresConnectionManager<postgres::NoTls>;
pub type Pool = r2d2::Pool<ConnectionManager>;
pub type PoolConn = r2d2::PooledConnection<ConnectionManager>;

pub fn initialize_connection_pool() -> Pool {
    let r2d2_manager = ConnectionManager::new(
        format!(
            "host={} port={} dbname={} user={} password={}",
            std::env::var("DB_HOST").unwrap(),
            std::env::var("DB_PORT").unwrap(),
            std::env::var("DB_NAME").unwrap(),
            std::env::var("DB_USER").unwrap(),
            std::env::var("DB_PASS").unwrap(),
        ).parse().expect("Postgres config error"),
        postgres::NoTls,
    );
    Pool::builder()
        .build(r2d2_manager)
        .expect("R2D2 pool error")
}